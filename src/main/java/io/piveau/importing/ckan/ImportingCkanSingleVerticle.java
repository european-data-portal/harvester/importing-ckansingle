package io.piveau.importing.ckan;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.piveau.importing.ckan.response.CkanArrayResult;
import io.piveau.importing.ckan.response.CkanError;
import io.piveau.importing.ckan.response.CkanResponse;
import io.piveau.pipe.connector.PipeContext;
import io.piveau.utils.Hash;
import io.piveau.utils.JenaUtils;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.RDF;

import java.util.concurrent.atomic.AtomicInteger;

public class ImportingCkanSingleVerticle extends AbstractVerticle {

    public static final String ADDRESS = "io.piveau.pipe.importing.ckansingle.queue";

    private WebClient client;

    @Override
    public void start(Future<Void> startFuture) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);
        client = WebClient.create(vertx);
        startFuture.complete();
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();
        pipeContext.log().info("Import started.");

        JsonNode config = pipeContext.getConfig();
        String address = config.path("address").asText();
        HttpRequest<Buffer> request = client.getAbs(address + "/api/action/package_list")
                .addQueryParam("offset", "0")
                .expect(ResponsePredicate.SC_OK);

        request.send(ar -> {
            if (ar.succeeded()) {
                CkanResponse ckanResponse = new CkanResponse(ar.result().bodyAsJsonObject());
                if (ckanResponse.isError()) {
                    CkanError ckanError = ckanResponse.getError();
                    pipeContext.setFailure(ckanError.getMessage());
                } else {
                    CkanArrayResult ckanResult = ckanResponse.getArrayResult();
                    JsonArray result = ckanResult.getContent();
                    AtomicInteger counter = new AtomicInteger(0);
                    result.forEach(name -> fetchDataset(name.toString(), result.size(), counter, pipeContext));
                    pipeContext.log().info("Import metadata finished");
                    vertx.setTimer(5000, t -> pipeContext.setResult(result.encodePrettily(), "application/json", new ObjectMapper().createObjectNode().put("content", "identifierList")).forward(client));
                }
            } else {
                pipeContext.setFailure(ar.cause());
            }
        });
    }

    private void fetchDataset(String name, int total, AtomicInteger counter, PipeContext pipeContext) {
        JsonNode config = pipeContext.getConfig();
        String address = config.path("address").asText();
        HttpRequest<Buffer> request = client.getAbs(address + "/dataset/" + name + ".rdf").expect(ResponsePredicate.SC_OK);
        request.send(ar -> {
            if (ar.succeeded()) {
                String dataset = ar.result().bodyAsString();
                String hash = Hash.asHexString(dataset);
                ObjectNode dataInfo = new ObjectMapper().createObjectNode()
                        .put("total", total)
                        .put("counter", counter.incrementAndGet())
                        .put("identifier", name)
                        .put("hash", hash);
                Model model = JenaUtils.read(dataset.getBytes(), Lang.RDFXML);
                ResIterator it = model.listResourcesWithProperty(RDF.type, DCAT.Dataset);
                if (it.hasNext()) {
                    Resource res = it.next();
                    Model extracted = JenaUtils.extractResource(res);
                    pipeContext.setResult(JenaUtils.write(extracted, Lang.NTRIPLES), "application/n-triples", dataInfo).forward(client);
                    pipeContext.log().info("Data imported: {}", dataInfo);
                } else {
                    pipeContext.log().error("Extracting resource: {}", dataInfo);
                }
            } else {
                pipeContext.log().error("Fetching dataset " + name, ar.cause());
            }
        });
    }

}
