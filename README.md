# importing ckan single
Microservice for importing from ckan source and feeding a pipe.

The service is based on the [pipe-connector](https://gitlab.com/european-data-portal/harvester/pipe-connector) library. Any configuration applicable for the pipe-connector can also be used for this service.

## Table of Contents
1. [Pipe Configuration](#pipe-configuration)
1. [Data Info Object](#data-info-object)
1. [Build](#build)
1. [Run](#run)
1. [Docker](#docker)
1. [License](#license)

## Pipe Configuration

_mandatory_

* `address` 

    Address of the source

## Data Info Object

* `total` 
    
    Total number of datasets

* `counter`
    
    The number of this dataset

* `identifier` 

    The unique identifier in the source of this dataset

## Build
Requirements:
 * Git
 * Maven
 * Java

```bash
$ git clone https://gitlab.com/european-data-portal/harvester/importing-ckansingle.git
$ cd importing-ckansingle
$ mvn package
```

## Run

```bash
$ java -jar target/importing-ckansingle-far.jar
```

## Docker

Build docker image:

```bash
$ docker build -t importing-ckansingle .
```

Run docker image:

```^bash
$ docker run -it -p 8080:8080 importing-ckansingle
```

## License

[Apache License, Version 2.0](LICENSE.md)
